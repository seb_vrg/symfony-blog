<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class,
                [
                    'label' => 'Nom',
                    // Contraintes de validations car le formulaire n'est pas mappé
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Vous ne pouvez être anonyme !'
                        ])
                    ]
                ]

            )
            ->add('email',EmailType::class,[
                'label' => 'Email',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Email obligatoire'
                    ]),
                    new Email([
                        'message' => 'Votre email est invalide'
                    ])
                ]
            ])
            ->add('subject',TextType::class,
                [
                    'label' => 'Sujet',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Le sujet est obligatoire !'
                        ])
                    ]
                ]

            )
            ->add('content',TextareaType::class,
                [
                    'label' => 'Votre message',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'Le message est obligatoire !'
                        ])
                    ]
                ]

            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
