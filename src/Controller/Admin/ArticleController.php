<?php


namespace App\Controller\Admin;


use App\Entity\Article;
use App\Entity\Comment;
use App\Form\ArticleType;
use App\Form\SearchArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ArticleController
 * @package App\Controller\Admin
 *
 * @Route("/article")
 */
class ArticleController extends AbstractController
{

    /**
     * @Route("/")
     * @param Request $request
     * @param ArticleRepository $articleRepository
     * @return Response
     */
    public function index(Request $request,ArticleRepository $articleRepository)
    {
        $searchForm =$this->createForm(SearchArticleType::class);
        $searchForm->handleRequest($request);



        $articles =$articleRepository->search((array)$searchForm->getData());


        return $this->render('admin/article/index.html.twig',[
            'articles' =>$articles,
            'search_form' => $searchForm->createView()
        ]);
    }

    /**
     * @Route("/edition/{id}",defaults={"id": null}, requirements={"id" : "\d+"})" )
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param $id
     * @return Response
     * @throws \Exception
     */
    public function edit(Request $request,EntityManagerInterface $manager,$id)
    {
        $originalImage = null;

        if (is_null($id)) {
            $article = new Article();
            $article->setAuthor($this->getUser());

        }else {
            $article = $manager->find(Article::class,$id);


            if (is_null($article)) {
                throw new NotFoundHttpException();
            }



            if (!is_null($article->getImage())) {

                $originalImage = $article->getImage();
                //on set l'image avec un objet File pour le champ du formulaire
               $article->setImage(
                   new File($this->getParameter('upload_dir').$originalImage)
               );
            }
        }

        $form = $this->createForm(ArticleType::class,$article);
        $form->handleRequest($request);
        if ($form->isSubmitted() )
        {

            if ($form->isValid()) {
                $datePublication =new \DateTime();

                /** @var UploadedFile $image */
                $image = $article->getImage();

                if (!is_null($image)) {
                    $filename = uniqid().'.'.$image->guessExtension();

                    $image->move($this->getParameter('upload_dir'),$filename);

                    $article->setImage($filename);
                    // suppression de l'ancienne image si nouvelle image
                    if (!is_null($originalImage)) {
                        unlink($this->getParameter('upload_dir'),$originalImage);
                    }
                } else {
                    $article->setImage($originalImage);
                }


                $article->setPublicationDate($datePublication);

                $manager->persist($article);
                $manager->flush();
                $this->addFlash('success',"L'article est enregistré !");
                return $this->redirectToRoute('app_admin_article_index');
            } else {
                $this->addFlash('error',"Le formulaire contient des erreurs !");
            }


        }


        return $this->render('article/edit.html.twig',[
            'form'=>$form->createView(),
            'original_image' => $originalImage
        ]);


    }


    /**
     * @Route("/comments/{id}")
     * @param Article $article
     * @param CommentRepository $commentRepository
     * @return Response
     */
    public function comments(Article $article,CommentRepository $commentRepository)
    {
        $listComments = $commentRepository->findBy([
            'article' => $article
        ],[
            'publicationDate' => 'DESC'
        ]);
        $articleTitle = $article->getTitle();
        return $this->render('admin/article/manage_comments.html.twig',[
            'comments' => $listComments,
            'article_title' => $articleTitle,
            'article' => $article
        ]);
    }


    /**
     * @Route("/deleteComment/{id}")
     * @param Comment $comment
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function deletecomment(Comment $comment,EntityManagerInterface $manager)
    {

        $authorComment = $comment->getAuthor();

        $manager->remove($comment);
        $manager->flush();
        $this->addFlash('success',"Le commentaire de <strong>$authorComment</strong> a été supprimé.");
        return $this->redirectToRoute('app_admin_article_index');
    }

    /**
     * @Route("/suppression/{id}")
     * @param Article $article
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Article $article,EntityManagerInterface $em)
    {

        $articleTitle = $article->getTitle();
        $em->remove($article);
        $em->flush();
        $this->addFlash('success',"L'article $articleTitle a été supprimé");

        return $this->redirectToRoute('app_admin_article_index');
    }

}