<?php


namespace App\Controller\Admin;


use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
/**
 * Class CategoryController
 * @package App\Controller\Admin
 * préfixe de route pour toutes les routes définies dans la classe
 * @Route("/categorie")
 */
class CategoryController extends AbstractController
{


    /**
     * @Route("/")
     * @param CategoryRepository $categorie
     * @return Response
     */
    public function index(CategoryRepository $categorie)
    {

      //  $list = $categorie->getListCategory();
        //ou
        $tri = $categorie->findBy([],['name'=>'ASC']);
        return $this->render("admin/category/index.html.twig",[
            'list' =>$tri
        ]);
    }


    /**
     * @Route("/edition/{id}",defaults={"id": null}, requirements={"id" : "\d+"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(Request $request,EntityManagerInterface $manager,$id)
    {

        if (is_null($id)) { // CREATION
            $category = new Category();
        } else {           // MODIFICATION
            $category = $manager->find(Category::class,$id);

            if(is_null($category)){
                throw new NotFoundHttpException();
            }
        }


        $form = $this->createForm(CategoryType::class,$category);
        $form->handleRequest($request);


        if ($form->isSubmitted() )
        {
            if ($form->isValid()) {

                $manager->persist($category);
                $manager->flush();
                $this->addFlash('success','La catégorie est enregistrée !');

                return $this->redirectToRoute('app_admin_category_index');
            } else {
                $this->addFlash('error','<b>Le formulaire contient des erreurs !</b>');
            }
        }

        return $this->render('admin/category/edit.html.twig',[
            'form'=>$form->createView()
        ]);


    }

    /**
     * @Route("/suppression/{id}")
     * @param Category $category
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function delete(Category $category,EntityManagerInterface $em)
    {
        if (!empty($category->getArticles()))
        {
            $this->addFlash('warning',"<b>Impossible </b>,la catégorie contient des articles !");

        } else {

        $categoryName = $category->getName();
        $em->remove($category);
        $em->flush();

        $this->addFlash('success',"La catégorie $categoryName a été supprimée");
        }

        return $this->redirectToRoute('app_admin_category_index');
    }
}