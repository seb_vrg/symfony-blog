<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/categorie")
 * Class CategoryController
 * @package App\Controller
 *
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/{id}")
     * @param Category $category
     * @return Response
     */
    public function index(ArticleRepository $articleRepository,Category $category)
    {
        $articles = $articleRepository->findBy(
            [

                'category' => $category
        ],
        [
            'publicationDate'=> 'DESC'
        ],3);

        return $this->render('category/index.html.twig', [

            'category' => $category,
            'articles' => $articles
        ]);
    }




    public function menu(CategoryRepository $repository)
    {
        $categories = $repository->findBy(
            [],
            ['name' => 'ASC']
        );
        return $this->render('category/menu.html.twig',
                [
                    'categories' => $categories
                ]
            );
    }
}
