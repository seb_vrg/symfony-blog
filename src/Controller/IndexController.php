<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index()
    {
        return $this->render('index/index.html.twig');
    }


    /**
     * @Route("/contact")
     * @param Request $request
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function contact(Request $request,\Swift_Mailer $mailer)
    {

        $form = $this->createForm(ContactType::class);



        //si il s'agit d'un utiliasteur connecté
        if (!is_null($this->getUser())) {
            //on prérempli le formulaire avec le nom de l'utilisateur (to_string)
            $form->get('name')->setData($this->getUser());
            $form->get('email')->setData($this->getUser()->getEmail());
        }
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) {

                $data = $form->getData();
                $contactEmail = $this->getParameter('contact_email');
                $mail = $mailer->createMessage();

                $mailBody = $this->renderView(
                    'index/contact_body.html.twig',[
                        'data' => $data
                    ]
                );
                $mail->setSubject('Nouveau message sur votre blog')
                     ->setFrom($contactEmail)
                     ->setTo($contactEmail)
                     ->setBody($mailBody,'text/html')
                     ->setReplyTo($data['email'])
                    ;
                $mailer->send($mail);

                $this->addFlash('success',"Votre message est envoyé");

            } else {
                $this->addFlash('error',"Le formulaire contient des erreurs");
            }
        }

        return $this->render(
            'index/contact.html.twig',[
                'form'=>$form->createView()
            ]
        );
    }

}
