<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/article")
 * Class ArticleController
 * @package App\Controller
 *
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/{id}")
     * @param Article $article
     * @param Request $request
     * @param CommentRepository $commentRepository
     * @param EntityManagerInterface $manager
     * @return Response
     * @throws \Exception
     */
    public function index(Article $article,Request $request,CommentRepository $commentRepository,EntityManagerInterface $manager)
    {
        $commment = new Comment();

        $listComment = $commentRepository->findBy([
            'article' => $article
        ],[
            'publicationDate' => 'DESC'
        ]);


        $formComment = $this->createForm(CommentType::class,$commment);
        $formComment->handleRequest($request);

        if ($formComment->isSubmitted()) {
            
            if ($formComment->isValid()) {

                $dateComment = new \DateTime;


                $commment->setPublicationDate($dateComment);
                $commment->setAuthor($this->getUser());
                $commment->setArticle($article);

                $manager->persist($commment);
                $manager->flush();

                $this->addFlash('success',"Le commentaire a été enregistré !");
                return $this->redirectToRoute('app_article_index',[
                    'id' => $article->getId()
                ]);

            } else {
                $this->addFlash('error',"Le formulaire contient des erreurs !");
            }
        }
            

        return $this->render('article/index.html.twig', [
           'article' => $article,
            'form_comment' => $formComment->createView(),
            'comments' => $listComment
        ]);
    }
}
